# Desafio Frontend
Crie uma aplicação utilizando as Api's do portal Developers da Marvel. As tarefas são as seguintes: 

##### Lista dos quadrinhos do Homem de Ferro ###
Crie uma lista de todas as edições de quadrinhos que o Homem de Ferro aparece ordenado por data. Cada item da lista deve conter imagem, titulo, descrição e data da edição. 
Utilize Scroll infinito para carregar novos itens e LazyLoad para o carregamento das imagens.

##### Detalhe da Edição ###
Ao clicar em um item da lista, apresente os detalhes da edição. O detalhe deve conter a maior quantidade possível de informações a respeito.

#### Requisitos:
 - Utilize HTML5 + CSS3 com Flex.
 - Utilize Angular, React ou VueJS no desenvolvimento.
 - Utilize JQuery.
 - Não utilize Bootstrap ou outros frameworks do tipo.
 - O Layout precisa ser responsivo.
 - Descreva no README os passos para execução do seu projeto.
 - Deixe seu repositório público para analise do Pull Request.

#### Ganha mais pontos:
 -  Criação de testes instrumentados.
 -  Automação com Grunt ou Gulp.
 -  Otimizações para aumentar a velocidade de renderização.

#### Submissão
 - Criar um fork desse projeto e entregar via Pull Request.

#### Prazo de Entrega
 - 4 Dias.

#### Dados de acesso a api da Marvel
 - Portal: https://developer.marvel.com/
 - Documentação: https://developer.marvel.com/docs

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
